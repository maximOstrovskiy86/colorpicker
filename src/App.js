import React, {useState} from "react";
import "./App.scss";

import ColorPicker from "./componets/ColorPicker";

const colorSettings = [
    {label: 'yellow', color: '#ebb146'},
    {label: 'red', color: '#e0222e'},
    {label: 'green', color: '#17a458'},
    {label: 'blue', color: '#1dafea'}
]

export default function App() {

    const [color, setColor] = useState(colorSettings[0].color);

    return (
        <>
            <ColorPicker colors={colorSettings} value={color} onChange={setColor}/>
        </>
    )
}


