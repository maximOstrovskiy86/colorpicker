import React, {useState, useEffect} from 'react';
import rgbHex from 'rgb-hex';

function hexToRgb(hex) {
    const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function (m, r, g, b) {
        return r + r + g + g + b + b;
    });

    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function ColorPicker(props) {
    const [red, setRed] = useState(0);
    const [green, setGreen] = useState(0);
    const [blue, setBlue] = useState(0);

    const [showColor, setShowColor] = useState(false);
    const [showColorList, setShowColorList] = useState(false);

    const [currentColor, setCurrentColor] = useState(props.value);

    useEffect(() => {
        setColors();
    }, []);

    useEffect(() => {
        const hex = rgbHex(`rgb(${red}, ${green}, ${blue})`);
        setCurrentColor(`#${hex}`);
    }, [red, green, blue]);

    function setColors() {
        const rgb = hexToRgb(props.value);
        setRed(rgb?.r);
        setGreen(rgb?.g);
        setBlue(rgb?.b);
    }

    const handleClickShow = () => {
        setColors();
        setShowColor(!showColor);
        setShowColorList(false);
    }

    const handleClickShowList = () => {
        setShowColorList(!showColorList);
        setShowColor(false);
    }

    const handleClickClose = () => {
        setColors();
        setShowColor(false);
    }

    const handleOnOk = () => {
        const hex = rgbHex(`rgb(${red}, ${green}, ${blue})`);
        props.onChange(hex);
        setShowColor(showColor => !showColor);
    }

    const close = (event) => {
        let close = event.target.dataset.show;
        if (close) {
            setColors();
            setShowColor(false);
            setShowColorList(false);
        }
    }

    const handleClickHex = (item) => {
        if (item) {
            props.onChange(item.color);
            setCurrentColor(item.color);
            console.log(item.color)
        }
        setShowColorList(false);
    }

    return (
        <>
            <div className="container" id="wrapper" data-show="true" onClick={close}>
                <div className="main" data-show="true">
                    <div className="color-picker">
                        <div><span className="color-hex">{currentColor.toUpperCase()}</span></div>
                        <div><span className="color-item-rgb" style={{backgroundColor: currentColor}}
                                   onClick={handleClickShow}> </span></div>
                        <div><span className="color-item-list" onClick={handleClickShowList}> </span></div>
                    </div>
                    {showColor && (
                        <div className="wrap-list list-color-rgb">
                            <ul className="">
                                <li>
                                    <label><b>Red</b></label>
                                    <input type="range" onChange={(e) => {
                                        setRed(e.target.value)
                                    }} min="0" max="255" value={red}/>
                                </li>
                                <li>
                                    <label><b>Green</b></label>
                                    <input type="range" onChange={(e) => {
                                        setGreen(e.target.value)
                                    }} min="0" max="255" value={green}/>
                                </li>
                                <li>
                                    <label><b>Blue</b></label>
                                    <input type="range" onChange={(e) => {
                                        setBlue(e.target.value)
                                    }} min="0" max="255" value={blue}/>
                                </li>
                            </ul>
                            <div className="footer-list">
                                <button onClick={handleClickClose}>Cancel</button>
                                <button onClick={handleOnOk}>Ok</button>
                            </div>
                        </div>
                    )}
                    {showColorList && (
                        <ul className="list-color-hex">
                            {props.colors.map((item, index) => {
                                return <li onClick={() => handleClickHex(item)}
                                           key={index}>{item.label.toUpperCase()}<span
                                    className="color-item-hex" style={{backgroundColor: item.color}}> </span></li>
                            })}
                        </ul>
                    )}
                </div>
            </div>
        </>
    );
}

export default ColorPicker;
